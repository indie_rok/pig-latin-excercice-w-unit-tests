require_relative 'PigLatin.rb'

describe PigLatin do
  let(:pig_latin) { PigLatin.new(string) }
  let(:string) { "hello world" }

  describe ".phrase" do
    subject { pig_latin.phrase }
    it { expect(subject).to eq("hello world") }
    it "can reset strings" do
      pig_latin
      pig_latin.phrase = "world hello"
      expect(subject).to eq("world hello")
    end
  end

  describe ".translate" do
    subject { pig_latin.translate }
    it(:class) { expect(pig_latin.phrase).to be_a_kind_of(String) }

    context "empty string" do
      let(:string) { "" }
      it { expect(pig_latin.translate).to eq(nil) }
    end

    context "start with vowel" do
      let(:string) { "hola" }
      it { expect(subject).to eq("olahay") }

      context "ends with vowel too" do
        let(:string) { "apple" }
        it { expect(subject).to eq("appleyay") }
      end

      context "ends with y" do
        let(:string) { "any" }
        it { expect(subject).to  eq("anynay") }
      end
    end

    context "the  word start with one consontant" do
      let(:string) { "hello" }
      it { expect(subject).to  eq("ellohay") }
    end

    context "word start with multiple consonants" do
      context "known" do
        let(:string) { "known" }
        it { expect(subject).to  eq("ownknay") }
      end
      context "special" do
        let(:string) { "special" }
        it { expect(subject).to  eq("ecialspay") }
      end
    end

    context "to many word, problems" do
      let(:string) { "hello world" }
      it { expect(subject).to  eq("ellohay orldway") }

      context "anoying '-' words" do
        let(:string) { "well-being" }
        it { expect(subject).to  eq("ellway-eingbay") }
      end
    end

    context "uppercase" do
      context "Bill" do
        let(:string) { "Bill" }
        it { expect(subject).to  eq("Illbay") }
      end
      context "Andrew" do
        let(:string) { "Andrew" }
        it { expect(subject).to  eq("Andreway") }
      end
    end

    context "dont forget puntuation" do
      context "fantastic!" do
        let(:string) { "fantastic!" }
        it { expect(subject).to  eq("antasticfay!") }
      end
      context "hiper hard test because YOLO" do
        let(:string) { "Three things: one, two, three." }
        it { expect(subject).to  eq("Eethray ingsthay: oneyay, otway, eethray.") }
      end
    end
  end
end
