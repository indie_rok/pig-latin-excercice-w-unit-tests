class PigLatin

  def initialize string
    @phrase = string
  end

  def phrase=(string)
    @phrase = string
  end

  def phrase
    @phrase
  end

  def translate
    return nil if (@phrase.empty?)

    array = @phrase.split(" ")

    finalArray = []

    array.each do |word|
      finalArray.push word_parser(word)
    end

    finalArray = finalArray.join(" ")

    return finalArray
  end





  def word_parser word
    specialWords = word.split("-")

    finalWords = []

    vowels = ['a','e','i','o','u','A','E','I','O','U']

    lowerConsonants = ('a'..'z').to_a
    upperConsonants = ('A'..'Z').to_a

    consonants = (lowerConsonants + upperConsonants) - vowels

    specialWords.collect! do |word|
      punctuation = word.slice!(/\W/)

      if vowels.include? word[0]
        case word[-1]
        when /[aeiou]/
          word += "yay"
        when "y"
          word += "nay"
        else
          word += "ay"
        end
      else
        consonants = consonantParser(word)
        word = removeCharFromString(word,consonants) + consonants + "ay"
      end

      if (punctuation)
        word << punctuation
      end

      word
    end


    return specialWords.join("-")
  end

  def consonantParser word

    array = word.chars

    i = 0

    while i < array.length
      if isVowel(array[i])
         return array[0...i].join
      end
      i += 1
    end

    return array.join

  end

  def removeCharFromString word,lettersToRemove
    t = []

    lettersToRemove.chars.each do |char|
      word.slice!(word.index(char))
    end

    return word
  end



  def isVowel char
    vowels = ['a','e','i','o','u','A','E','I','O','U']

    if vowels.include? char
      return true
    else
      return false
    end
  end
end
